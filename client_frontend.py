import tkinter as tk
import threading, time
from client import ChatClient
from chatprotocol import ChatProtocol

username_value = ""
MIN_LEN_USERNAME = 3
TIME_FORMAT="(%H:%M:%S)"

DEBUG_FLAG=True


class ClientFrontend(tk.Tk, ChatClient):
    def __init__(self,server_ip="localhost", server_port=9999, *args, **kwargs):
        super(ClientFrontend, self).__init__(*args, **kwargs)
        ChatClient.__init__(self, server_ip, server_port)
        
        self.createHeader().pack(fill=tk.X)
        self.createChatFrame().pack(fill=tk.BOTH, expand=1)
        self.disableChat()
        self.clientThread=None
        self.threadAliveFlag=False

    """ GUI METHODS """

    def createHeader(self):
        header_frame = tk.Frame(self)

        self.username_label = tk.Label(header_frame, text="Username:")
        self.username_entry = tk.Entry(header_frame, textvariable=username_value)
        self.username_entry.bind("<Return>", lambda _ : self.registerOnServer())
        if(DEBUG_FLAG):
            self.username_entry.insert(tk.END, "Mario")
        # self.server_connect_btn = tk.Button(header_frame, text="Connect", command=self.register)   
        self.server_connect_btn = tk.Button(header_frame, text="Connect", command=self.registerOnServer)   

        self.username_label.pack(side=tk.LEFT)
        self.username_entry.pack(side=tk.LEFT, fill=tk.X, expand=1)
        self.server_connect_btn.pack(side=tk.LEFT)

        return header_frame

    def createChatFrame(self):
        chat_frame = tk.Frame(self)
        
        self.chatBox = tk.Text(chat_frame)
        self.chatBox.pack(fill=tk.BOTH, expand=1)

        self.messageLabel = tk.Label(chat_frame, text="Message:")
        self.messageLabel.pack(side=tk.LEFT)

        self.messageEntry = tk.Entry(chat_frame, text="Message:")
        self.messageEntry.pack(side=tk.LEFT, fill=tk.X, expand=True)
        self.messageEntry.bind("<Return>", lambda _ : self.sendMessageFromGui())

        self.messageButton = tk.Button(chat_frame, text="Send", command=self.disableChat)
        self.messageButton.pack(side=tk.LEFT)

        return chat_frame

    def enableChat(self):
        self.chatBox.config(bg="white", state=tk.DISABLED)
        self.messageEntry.config(state=tk.NORMAL)
        self.messageButton.config(state=tk.NORMAL, command=self.sendMessageFromGui)
        self.username_entry.config(state=tk.DISABLED)
        self.server_connect_btn.config(text="Disconnect", command=self.disconnect)

    def disableChat(self):
        self.chatBox.config(bg="gainsboro", state=tk.DISABLED)
        self.messageEntry.config(state=tk.DISABLED)
        self.messageButton.config(state=tk.DISABLED)
        self.username_entry.config(state=tk.NORMAL)
        self.server_connect_btn.config(text="Connect", command=self.registerOnServer)

    def openPopup(self, message):
        win = tk.Toplevel()
        win.wm_title("Message")
        msg = tk.Message(win, text=message, width=300)
        msg.pack()
        btn = tk.Button(win, text="Close", command=win.destroy)
        btn.pack()

    
    def addToChat(self, username, message):
        print("Addam v chat")
        _string = "{} {}:{}\n".format(time.strftime(TIME_FORMAT),
                                        username,
                                        message)
        self.chatBox.configure(state=tk.NORMAL)
        self.chatBox.insert(tk.END, _string)
        self.chatBox.configure(state=tk.DISABLED)
        self.chatBox.update_idletasks()
        # lenga = self.chatBox.count("displaylines")
        # print(lenga)
        last_index = self.chatBox.index("end-1c")
        self.chatBox.see(last_index)
        # print(lenga)
        # print(self.chatBox.dlineinfo(str(float(30))))

    def testMe(self):
        self.addToChat("parsed[1]", "parsed[2]")

    """ END GUI """


    # def initalizeClient(self, host="localhost", port=9999):
    #     self.client = ChatClient(host, port)

    # def getClient(self):
    #     return self.client

    def connectToServer(self):
        if(not self.isClientRunning()):
            self.startClientOnThread()

    def sendMessageFromGui(self):
        message = self.messageEntry.get().strip()
        print("MGGGAGA {}".format(message))
        if(len(message) > 0):
            self.parseMessage(message)
            self.messageEntry.delete(0, tk.END)

    def disconnect(self):
        self.disableChat()
        self.clearRunThread()
        self.close()
        # self.clientThread.join()

    def registerOnServer(self):
        # TODO check username length
        if(not self.isConnected()):
            self.initSocket()
            print("socket not connected")
            status = self.connectSocket()
            if(not status):
                self.openPopup("Cannot open socket to server")
                return

        if(not self.isClientRunning()):
            self.startClientOnThread()
            # maybe add string to return        
        print("Getting uusername:")
        name = self.username_entry.get().strip()
        print("Sending register name: {}".format(name))

        # TODO add username len check
        self.setUsername(name)    

        super().registerOnServer()

    # def handleMessage(self, message):

    """ This function handles data recieve callback """
    # TODO data parsing
    def dataReciveCallback(self, data):
        super().dataReciveCallback(data)
        print("FRONTEND")
        print(self.command_buffer)
        while(not self.isMessageBufferEmpty()):
            print("Not emptry")
            print(self.command_buffer)
            parsed = self.getNextCommandTuple()
            command = parsed[0]
            print("Parsed: ")
            print(parsed)

            if(command == ChatProtocol.COMMAND_DICT["register_success"]):
                print("Auth successful")
                self.isAuth = True
                self.enableChat()

            elif(command == ChatProtocol.COMMAND_DICT["register_fail"]):
                self.openPopup("Username already is use")
                

            elif(command == ChatProtocol.COMMAND_DICT["register_auth"]):
                self.openPopup("You are registered already")

            else:
                self.addToChat(parsed[1], parsed[2])
        print("Prazno je ")
        print(self.command_buffer)

    def run(self):
        status = super().run()
        if(status is False):
            self.disconnect()
            self.disableChat()
            self.openPopup("Lost connection to server")
            

    def startClientOnThread(self):
        self.clientThread = threading.Thread(target=self.run)
        # Daemon - so threads are killed then main program is exited
        self.clientThread.setDaemon(True)
        self.threadAliveFlag=True
        print("Starting thread")
        try:
            result = self.clientThread.start()
        except Exception as e:
            print("Exception raised while try to run thread")
            print(e)
        print("thread finished")

    # def sendData(self, data):
    #     self.getClient().sendData(data)

    def isClientRunning(self):
        if(isinstance(self.clientThread, threading.Thread)):
            return self.clientThread.isAlive()
        else:
            return False


def addToChat(d):
    print("Addam v chat")
    T.configure(state=tk.NORMAL)
    T.insert(tk.END, "yean, bro")
    T.configure(state=tk.DISABLED)
    print(T)
    print(d)

def connectToServer():
    print("Button pressed")
    username = username_entry.get()
    if(len(username) < MIN_LEN_USERNAME):
        openPopup("Username to short")
        return
    c.setUsername(username)
    status = c.register()
    print("Status response")
    print(status)
    

# tk.mainloop()
    if(status):
        # enable 
        print("Oke")
        username_entry.configure(state=tk.DISABLED)
    else:
        openPopup("Username already exists")

    
def openPopup(message):
    win = tk.Toplevel()
    win.wm_title("Message")
    msg = tk.Message(win, text=message, width=300)
    msg.pack()
    btn = tk.Button(win, text="Close", command=win.destroy)
    btn.pack()

main_program = ClientFrontend(server_ip="localhost", server_port=9999)
# main_program.initalizeClient()

# Add support for ctrl-a - select all
main_program.bind_class("Entry", "<Control-a>", lambda e: e.widget.selection_range(0, tk.END))
main_program.mainloop()

