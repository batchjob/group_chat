from enum import Enum
import time

class LoggerLevels(Enum):
    INFO = 1,
    DEBUG = 2,
    WARNING = 3,
    ERROR = 4

class Logger():

    # Logger format for strptime method
    TIME_FORMAT="%d.%m.%Y. %H:%M:%S - "

    # File is path for logging file
    def __init__(self, log_file, writeToStdOut=False):
        self.log_file = log_file
        self.writeToStdOut=writeToStdOut
    
    def getLevel(self):
        return self.level
    
    def getTimeForLog(self):
        return time.strftime(Logger.TIME_FORMAT)

    def writeToStd(self, data):
        print(self.getTimeForLog() + str(data))

    def writeToFile(self, data):
        with open(self.log_file, 'a+') as f:
            f.write(self.getTimeForLog() + str(data)+'\n')

    def writeData(self, data):
        self.writeToFile(data)
        if(self.writeToStdOut is True):
            self.writeToStd(data)
        
