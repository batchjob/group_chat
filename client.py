import socket
import sys
import time
import chatsocket
import threading
from chatprotocol import ChatProtocol, CezarCipher
from collections import deque

g_lock = threading.Lock()

HOST, PORT = "localhost", 9999
data = " ".join(sys.argv[1:])

register_string="/r "

TIMEOUT = 1

class ChatClient():

    def __init__(self, server_ip, server_port):
        print("Init clinet: ip {} port: {}".format(server_ip, server_port))
        self.server_ip = server_ip
        self.server_port = server_port
 
        self.initSocket()

        self.username = ""
        self.isAuth = False
        self.recieveBuffer = ""
        # needs refactor
        # its list not dict
        self.callback_dict = list()
        self.runningThread=None

        self.command_buffer=deque()

    def initSocket(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setblocking(False)
        self.socket.settimeout(TIMEOUT)

    def appendToBuffer(self, command):
        self.command_buffer.append(command)

    def getFirstCommandFromBuffer(self):
        if(not self.isMessageBufferEmpty()):
            return self.command_buffer.popleft()
        return None

    def getNextCommandTuple(self):
        response = list()
        curr_command = self.getFirstCommandFromBuffer()
        if(curr_command is None):
            return response

        print("Moj commanda je: ")
        print(curr_command)
        while(curr_command != ChatProtocol.COMMAND_DICT["end_command"]):
            response.append(curr_command)
            curr_command = self.getFirstCommandFromBuffer()
            if(curr_command is None):
                break
        return response

    def isMessageBufferEmpty(self):
        return len(self.command_buffer) < 1

    def setUsername(self, username):
        self.username = username

    def getUsername(self):
        return self.username

    def registerOnServer(self):
        print("Sending register function")        

        print(self.getUsername())
        buffer = ChatProtocol.buildStringToSend(ChatProtocol.COMMAND_DICT["register"], self.getUsername())
        if(buffer is not None):
            # maybe add string to return
            if(not self.sendData(buffer)):
                return False
            # check auth
            return True
        return False
            

    def dataReciveCallback(self, data):
        # parse data
        print("data recieved")
        print(data)
        _data = ChatProtocol.splitString(data)
        print("Data on callback")
        for command in _data:
            self.appendToBuffer(command)

        for func in self.callback_dict:
            func(data)


    def sendData(self, data):
        try:
            self.socket.sendall(data.encode("utf-8"))
            return True
        except BrokenPipeError as e:
            print("Server unreachable")
            return False

    def parseMessage(self, data):
        _data = data.split(" ")
        # TODO test This!!!!!



        print("Client printam split string")
        print(_data)
        if(_data[0] == ChatProtocol.COMMAND_DICT["hangman_start"] or 
            _data[0] == ChatProtocol.COMMAND_DICT["hangman_end"] or
            _data[0] == ChatProtocol.COMMAND_DICT["hangman_status"]):
            response = ChatProtocol.buildStringToSend(_data[0])
            self.sendData(response)
            return
        if(_data[0] == ChatProtocol.COMMAND_DICT["hangman_guess"]):
            if(len(_data) < 2):
                return
            response = ChatProtocol.buildStringToSend(_data[0], _data[1])
            self.sendData(response)
            return

        print("DATA: {}".format(_data))
        self.sendMessageToServer(data)

    
    def sendMessageToServer(self, message):
        
        response = ChatProtocol.buildStringToSend(ChatProtocol.COMMAND_DICT["user_toAll"], self.getUsername(), message)
        self.sendData(response)

    def isConnected(self):
        try:
            print("Pinging server")
            
            self.socket.sendall(ChatProtocol.buildStringToSend(ChatProtocol.COMMAND_DICT["keep_alive"]).encode("utf-8"))
            # _data = self.socket.recv(1)
            return True
        except socket.timeout:
            return True
        except BrokenPipeError:
            return False
        except Exception:
            return False

    def connectSocket(self):
        try:
            self.socket.connect((self.server_ip, self.server_port))
            return True
        except ConnectionRefusedError:
            print("Connection refused")
            return False
        except Exception as e:
            print("Client connection failed")
            print(e)
            return False

    def clearRunThread(self):
        if(self.runningThread and self.runningThread.isAlive()):
            self.keepMeAlive = False
            # self.runningThread.join()


    def run(self):
        # init socket and 
        # g_lock.acquire()
        print("Run started")
        # g_lock.release()

        self.runningThread = threading.currentThread()
        self.keepMeAlive = True


        # if(not self.isConnected()):
        #     print("socket not connected")
        #     return
        
        retry = 0
        while self.keepMeAlive:
            try:
                # if(not self.isConnected()):
                #     print("Socket is closed")
                #     break
                _data = self.socket.recv(1024)
                if(len(_data)):
                    print("I just recieved new data")
                    print(_data)
                    _data = CezarCipher.decode(_data.decode('utf-8'))
                    self.dataReciveCallback(_data)
                retry = 0
            except socket.timeout:
                # this is normal, no data recieved
                self.isConnected()
                continue

            except BrokenPipeError as e:
                retry = retry + 1
                if(retry > 4):
                    print("Cannot connect, exiting")
                    return False
                print("Pipe broken, retry: {}".format(retry))

            except OSError:
                return False
            
            except ConnectionError as e:
                print("Conn error")
                print(e)
                print(self.isConnected())
                return False
        return True

    def globalChatCallback(self, target):
        # register callback with one argument that recieves data
        self.callback_dict.append(target)

    def close(self):
        self.socket.close()
