# !/usr/bin/python
# -*- coding: utf-8 -*-
import socket
import threading
import socketserver
import time
import logger
from chatprotocol import ChatProtocol, CezarCipher

from collections import deque

from hangman import *


LOGFILE="server_log.txt"

server_logger = logger.Logger(LOGFILE, writeToStdOut=True) 

lock = threading.Lock()

ENCODING = 'utf-8'

DEBUG = True

class ThreadedTCPRequestHandler(socketserver.BaseRequestHandler):
    def handle(self):
        self.keepAlive = True
        server_logger.writeData("New client connected from " + str(self.client_address[0]) + ":" + str(self.client_address[1]))
        self.request.setblocking(False)
        self.request.settimeout(5.0)
        cur_thread = threading.current_thread()

        new_user = ChatUser(ChatUser.DUMMY_NAME, self.request)
        self.server.addUser(new_user)
        # test for keep-alive   
        while self.keepAlive:
            try:

                data = self.request.recv(1024)
                # self.server.sendAllTest()
                if(not len(data)):
                    # try to send
                    try:
                        self.request.send(ChatProtocol.COMMAND_DICT["keep_alive"].encode(ENCODING))
                        # reciveve ack, implement it
                    except socket.error:
                        server_logger.writeData("User disconnected")
                        self.server.removeUser(new_user)
                        self.keepAlive = False
                else:
                    if(DEBUG):
                        print("Data recieved from user {}".format(new_user.getUsername()))
                        print("Data:", data.decode(ENCODING))
                    self.server.addMessageToBuffer(self.server.parseMessage(data), new_user)
                    self.server.handleAllBufferMessagesForUser(new_user)
            except socket.timeout:
                continue
            except socket.error as e:
                self.keepAlive = False
                # print(e)
                break
        self.server.removeUser(new_user)
        print("User {} has disconnected".format(new_user.getUsername()))
        self.server.sendToAllUsersAsServer("User {} has disconnected".format(new_user.getUsername()))

class ChatUser():

    # User cannot be named this
    DUMMY_NAME = "__placeholder__"


    def __init__(self, username, socket, isAuth=False):
        self.username = str(username)
        self.socket = socket
        self.isAuth = False
        self.message_buffer = deque()

    def addCommandToBuffer(self, command):
        self.message_buffer.append(command)
    
    def getFirstCommandFromBuffer(self):
        if(self.isMessageBufferEmpty()):
            return None
        return self.message_buffer.popleft()

    def getNextCommandTuple(self):
        response = list()
        curr_command = self.getFirstCommandFromBuffer()
        if(curr_command is None):
            return response

        while(curr_command != ChatProtocol.COMMAND_DICT["end_command"]):
            response.append(curr_command)
            curr_command = self.getFirstCommandFromBuffer()
            if(curr_command is None):
                break
        return response

    def isMessageBufferEmpty(self):
        return len(self.message_buffer) < 1

    def getUsername(self):
        return self.username

    def setUsername(self, username):
        self.username = username

    def isAuthenticated(self):
        return self.isAuth

    def authUser(self):
        self.isAuth = True

    def sendDataToUser(self, data):
        try:
            self.socket.sendall(data.encode(ENCODING))
            return True
        except BrokenPipeError:
            # TODO graceful exit
            print("User disconnected")
        except Exception as e:
            print("General error while sending: ")
            print(e)
        return False

    def sendMessageToUser(self, user, message):
        
        response = ChatProtocol.buildStringToSend(ChatProtocol.COMMAND_DICT["user_toAll"], user.getUsername(), message)
        self.sendDataToUser(response)

    def closeUserSocket(self):
        self.socket.close()
    # check if socket is live

class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    pass

class ChatServer(ThreadedTCPServer):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user_list = list()
        self.allow_reuse_address=True

        self.server_user = ChatUser("SERVER", None, True)
        
        self.game=None

    def isGameStarted(self):
        if(self.game and self.game.isGameStarted()):
            return True
        return False

    def addUser(self, user):
        self.user_list.append(user)

    def removeUser(self, user):
        try:
            self.user_list.remove(user)
        except Exception as e:
            print("User already removed")

    def getAllUsernames(self):
        usernames = list()
        for user in self.user_list:
            usernames.append(user.getUsername())
        return usernames

    def getUsers(self):
        return self.user_list

    def requireAuth(fnc, user):
        def magic(*args, **kwargs):
            if(user.isAuthenticated()):
                return fnc(*args, **kwargs)
            return False
        return magic

    def isGameInProgress(self):
        if(self.game and not self.game.isGameFinished()):
            return True

        return False

    def startGame(self):
        if(self.isGameInProgress()):
            return False

        self.game = HangmanGame()


    def isUsernameAvaliable(self, username):
        return username != ChatUser.DUMMY_NAME and username not in self.getAllUsernames()

    def parseMessage(self, data):
        #input are raw bytes, not string
        # decode this in chat protocol
        _data = CezarCipher.decode(data.decode(ENCODING))
        _data = ChatProtocol.splitString(_data)
        if('' in _data):
            _data.remove('')
        if(DEBUG):
            print("Resolving some data: {}".format(_data))
        return _data

    def sendDataToUser(self, user, data):
        user.sendDataToUser(data)

    def sendMessageToUser(self, from_user, to_user, message):
        print("Sending message to {} from {}".format(to_user.getUsername(), from_user.getUsername()))
        response = ChatProtocol.buildStringToSend(ChatProtocol.COMMAND_DICT["user_toAll"], from_user.getUsername(), message)
        to_user.sendDataToUser(response)

    def sendMessageToUserFromServer(self, user, message):
        self.sendMessageToUser(self.server_user, user, message)

    def tryAuthUser(self, user, name):
        if(user.isAuthenticated()):
            print("User already auth")
            return ChatProtocol.buildStringRegisterAlreadyDone()
        
        if(self.isUsernameAvaliable(name)):
            print("User {} is authenicated".format(name))
            user.setUsername(name)
            user.authUser()
            self.sendToAllUsersAsServer("User {} has joined server!".format(name))
            return ChatProtocol.buildStringRegisterSuccess()

        print("Username taken")
        return ChatProtocol.buildStringRegisterFail()


    def sendToAllUsers(self, user, data):
        for u in self.getUsers():
            if(u.isAuthenticated()):
                u.sendMessageToUser(user, data)

    def sendToAllUsersAsServer(self, data):
        self.sendToAllUsers(self.server_user, data)

    def addMessageToBuffer(self, split_tuple, user):
        for command in split_tuple:
            user.addCommandToBuffer(command)

    def getNextCommandsFromUser(self, user):
        return user.getNextCommandTuple()

    def handleBufferForUser(self, user):
        _a = user.getNextCommandTuple()
        self.handleMessage(_a, user)

    def handleAllBufferMessagesForUser(self, user):
        while not user.isMessageBufferEmpty():
            self.handleBufferForUser(user)

    def formatGameStats(self):
        stats = self.game.getGameStats()
        string_list = list()
        string_list.append("Word : {}".format(stats['word']))
        string_list.append("Maximum points possible: {}".format(stats['maxpoints']))
        for items in stats['users']:
            for user, points in items.items():
                string_list.append("User {} has score: {}".format(user, points))
        return string_list

    def printGameStatsToAllUsers(self):
        for string_item in self.formatGameStats():
            self.sendToAllUsersAsServer(string_item)



    def handleMessage(self, split_tuple, user):
        
        command = split_tuple[0]

        # TODO make if else if

        if(command == ChatProtocol.COMMAND_DICT["keep_alive"]):
            return

        print("Message recieved from {}".format(user.getUsername()))
        print("Contents: {}".format(split_tuple))

        if(command == ChatProtocol.COMMAND_DICT["hangman_start"]):
            if(self.isGameStarted()):
                self.sendToAllUsers(self.server_user, "Game is already in progress")
                return
            self.game = HangmanGame()
            self.sendToAllUsers(self.server_user, "User {} has started a new game".format(user.getUsername()))
            self.sendToAllUsers(self.server_user, "Word: {}".format(self.game.getCurrentWord()))
            return

        if(command == ChatProtocol.COMMAND_DICT["hangman_end"]):
            if(self.isGameStarted()):
                self.game.flagGameEnd()
                self.sendToAllUsers(self.server_user, "Game was stopped by user {}".format(user.getUsername()))
                self.printGameStatsToAllUsers()
                return
            self.sendToAllUsers(self.server_user, "Game is not started")
            return
        
        if(command == ChatProtocol.COMMAND_DICT['hangman_status']):
            if(self.isGameStarted()):
                self.printGameStatsToAllUsers()                
                return
            self.sendToAllUsersAsServer("Game is not started")
            return

        # TODO: print game status
        if(command == ChatProtocol.COMMAND_DICT["hangman_guess"]):
            if(self.isGameStarted()):
                # Try guess
                if(len(split_tuple[1]) != 1):
                    self.sendMessageToUserFromServer(user, "Please enter only one letter")
                    return
                self.sendToAllUsers(self.server_user, "User {} tried to guess {}".format(user.getUsername(), split_tuple[1]))

                status = self.game.guessLetter(user.getUsername(), split_tuple[1])                
                
                if(status == LETTER_EXIST):
                    self.sendToAllUsers(self.server_user, "Letter {} was already guessed".format(split_tuple[1]))
                elif(status == LETTER_GUESSED):
                    self.sendToAllUsers(self.server_user, "User {} guessed letter {}!".format(user.getUsername(), split_tuple[1]))
                    if(self.game.isGameFinished()):
                        self.sendToAllUsers(self.server_user, "Game is finished!")
                        self.printGameStatsToAllUsers()
                        return
                else:
                    self.sendToAllUsers(self.server_user ,"Letter {} is not in word".format(split_tuple[1]))
                
                __p_str = self.game.getCurrentWord()
                self.sendToAllUsers(self.server_user, __p_str)
                
                return
            self.sendToAllUsers(self.server_user, "Game is not started")
            return


        if(command == ChatProtocol.COMMAND_DICT["register"]):
            status = self.tryAuthUser(user, split_tuple[1])
            self.sendDataToUser(user, status)
            return

        if(command == ChatProtocol.COMMAND_DICT["send_all"]):
            for u in self.getUsers():
                u.sendToAll(user, split_tuple[1])
            return

        if(command == ChatProtocol.COMMAND_DICT["user_toAll"]):
            self.sendToAllUsers(user, split_tuple[2])
            return            
            

if __name__ == "__main__":
    # Kasnije napraviti server classu
    HOST, PORT = "localhost", 9999


    # threadList = list()
    server = None
    while(server is None):
        try:
            server = ChatServer((HOST, PORT), ThreadedTCPRequestHandler)
        except Exception as e:
            time.sleep(1)
            print(e)
            print("Retrying to start")
    print("Server started")
    server_logger.writeData("Server started")
    with server:
        ip, port = server.server_address
        try:
            server.serve_forever()
        except KeyboardInterrupt:
            server_logger.writeData("Break pressed, quiting server")
            for u in server.user_list:
                u.closeUserSocket()
            server.shutdown()
            server.server_close()
        except Exception as e:
            print("Serving ex")
    print("Done")
