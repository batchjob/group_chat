DEBUG = True

# TODO make end of sequence
# and escape string

class ChatProtocol():
    """
    Class that implements protocol and builds strings
    """
    # SEND_ALL = "#A"
    # SEND_USER = "#U"
    # SEPARATOR = "|",
    # REGISTER = "#R"

    COMMAND_DICT = {
        "separator": "|",
        "end_command": "#end",
        "send_all": "#A",
        "send_user": "#U",
        "register": "#R", 
        "register_success": "#RS",
        "register_fail": "#RF",
        "register_auth": "#RA",
        "keep_alive": "#?",
        "ack": "#1",
        "user_toAll": "#ToAll",
        "user_number": "#NU"
    }

    HANGMAN_DICT = { 
        "hangman_start": "#GAMESTART",
        "hangman_end": "#GAMESTOP",
        "hangman_guess": "#G",
        "hangman_status": "#GS"
    }

    STRING_ESCAPE_KEY = "/"

    STRING_ESCAPE_LIST = (STRING_ESCAPE_KEY, COMMAND_DICT["separator"], "#")

    for k,v in HANGMAN_DICT.items():
        COMMAND_DICT[k]=v

    @staticmethod
    def buildStringToSend(send_context, *args):
        if(send_context not in ChatProtocol.COMMAND_DICT.values()):
            # command not initialised
            return None
        _string = send_context + ChatProtocol.COMMAND_DICT["separator"]
        for data in args:
            # Every data we escape
            _string = _string  + ChatProtocol.escapeString(str(data)) + ChatProtocol.COMMAND_DICT["separator"]
        _string = _string + ChatProtocol.stringForMessageEnd()
        # At the end, cypher it
        return CezarCipher.encode(_string)

    @staticmethod
    def stringForMessageEnd():
        return ChatProtocol.COMMAND_DICT["end_command"]  + ChatProtocol.COMMAND_DICT["separator"]

    @staticmethod
    def buildStringRegisterSuccess():
        return ChatProtocol.buildStringToSend(ChatProtocol.COMMAND_DICT["register_success"])
         

    @staticmethod
    def buildStringRegisterFail():
        return ChatProtocol.buildStringToSend(ChatProtocol.COMMAND_DICT["register_fail"])

    @staticmethod
    def buildStringRegisterAlreadyDone():
        return ChatProtocol.buildStringToSend(ChatProtocol.COMMAND_DICT["register_auth"])

    @staticmethod
    def escapeString(data):
        escaped_string_list = list()
        for letter in data:
            if(letter in ChatProtocol.STRING_ESCAPE_LIST):
                escaped_string_list.append(ChatProtocol.STRING_ESCAPE_KEY)
            escaped_string_list.append(letter)
        return "".join(escaped_string_list)

    @staticmethod
    def deescapeString(data):
        working_stack = [i for i in data]
        working_stack.reverse()

        result_string_list=list()

        while(len(working_stack)>0):
            char = working_stack.pop()
            if(char == ChatProtocol.STRING_ESCAPE_KEY and 
                len(working_stack) > 0):
                next_char = working_stack.pop()
                if(next_char in ChatProtocol.STRING_ESCAPE_LIST):
                    result_string_list.append(next_char)
                else:
                    result_string_list.append(char)
                    result_string_list.append(next_char)
            else:
                result_string_list.append(char)

        return("".join(result_string_list))


    @staticmethod
    def splitString(data):
        # Ručni split
        if(DEBUG):
            print("Encoded data: {}".format(data))
        result_string_list= list()
        working_stack = [i for i in data]
        working_stack.reverse()

        buffer_string_list=list()
        while(len(working_stack)>0):
            char = working_stack.pop()
            if(char == ChatProtocol.STRING_ESCAPE_KEY and 
                len(working_stack) > 0):
                next_char = working_stack.pop()
                if(next_char in ChatProtocol.STRING_ESCAPE_LIST):
                    buffer_string_list.append(char)
                    buffer_string_list.append(next_char)
            elif(char == ChatProtocol.COMMAND_DICT["separator"]):
                result_string_list.append("".join(buffer_string_list))
                buffer_string_list=list()
            else:
                buffer_string_list.append(char)
        if(len(result_string_list) > 2):
            # de-escape strings
            inner_list = [ChatProtocol.deescapeString(_string) for _string in result_string_list[1:-1]]
            final_data = [result_string_list[0],] + inner_list + [result_string_list[-1],]
            if(DEBUG):
                print("Decoded data: {}".format(final_data))
            return final_data
        if(DEBUG):
            print("Decoded data: {}".format(result_string_list))
        return result_string_list

""" Cezar crypt """
import sys

SHIFT_PLACES = 1210

class CezarCipher():

    @staticmethod
    def encode(string):
        string_holder=list()
        for letter in string:
            string_holder.append(chr((ord(letter) + SHIFT_PLACES) % sys.maxunicode))

        return "".join(string_holder)
    
    @staticmethod
    def decode(string):
        string_holder=list()
        for letter in string:
            string_holder.append(chr((ord(letter) - SHIFT_PLACES) % sys.maxunicode))

        return "".join(string_holder)

if __name__ == "__main__":
    import random
    string = "Hello"
    SHIFT_PLACES = random.randint(1, 10000000000)
    print("Shift : {}".format(SHIFT_PLACES))
    enc = CezarCipher.encode(string)
    print(enc)
    dec = CezarCipher.decode(enc)
    print(dec)
    # Unit test
    assert(string == dec)