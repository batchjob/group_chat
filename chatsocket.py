import socket

class ChatSocket(socket.socket):
    def __init__(self, blocking=False, timeout=1, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setblocking(blocking)
        self.settimeout(timeout)

    def checkIfAlive(self):
        try:
            self.sendall(bytes("#A", 'utf-8'))
            return True
        except socket.error as e:
            return False

    