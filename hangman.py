#!/usr/bin/python3

import random

WORD_LIST=[
    "kakao",
    "vampir",
    "garfield",
    "težina"
]

LETTER_GUESSED = 0
LETTER_EXIST = 1
LETTER_NOT_IN = 2

VERBOSE=True

def printf(data):
    if(VERBOSE):
        print(data)

class HangmanPlayer():
    def __init__(self, username):
        self.username = username
        self.score = 0

    def incrementScore(self, point_weight):
        printf("Adding points to {}".format(self.getUsername()))
        self.score = self.score + point_weight

    def getScore(self):
        return self.score

    def getUsername(self):
        return self.username

class HangmanGame():

    POINTS_FOR_LETTER=2

    def __init__(self, word=random.choice(WORD_LIST), users=list()):
        self.word = word
        printf("Word is {}".format(self.word))
        self.lettersInWord=list()
        for l in self.word:
            if l.lower() not in self.lettersInWord:
                self.lettersInWord.append(l.lower())

        self.guessedLetters=list()
        self.gameFinished=False
        self.users = users

        for u in self.users:
            u.score = 0

        # Callback definitions
        self.finishCallback=None
        self.guessCallback=None

    def addUser(self, username):
        _player = HangmanPlayer(username)
        self.users.append(_player)
        return _player

    def getUserByUsername(self, username):
        for user in self.users:
            if(user.getUsername() == username):
                return user
        return None

    def userExists(self, username):
        return (self.getUserByUsername(username) is not None)

    def getCurrentWord(self, separator=' '):
        temp_word = list()
        [temp_word.append(l) for l in self.word]
        for i, l in enumerate(temp_word):
            if(l not in self.guessedLetters):
                temp_word[i]="_"
        return separator.join(temp_word)

    def letterCheck(self, user, l):
        _user = None

        if(isinstance(user, str)):
            if(self.userExists(user)):
                _user = self.getUserByUsername(user)
            else:
                _user = self.addUser(user)
        else:
            if(self.userExists(user.getUsername())):
                _user = self.getUserByUsername(user.getUsername())
            else:
                _user = self.addUser(user.getUsername())
                           
        printf("User {} tried to guess letter {}".format(_user.getUsername(), l))
        
        if(l not in self.lettersInWord):
            return LETTER_NOT_IN
        if(l not in self.guessedLetters):
            printf("Letter guessed corrects")
            # better score, # of letters * points
            printf(self.guessedLetters)
            _user.incrementScore(HangmanGame.POINTS_FOR_LETTER)
            self.guessedLetters.append(l)
            if(self.gameStateCheck()):
                self.flagGameEnd()
            return LETTER_GUESSED
        else:
            return LETTER_EXIST

    def guessLetter(self, user, l):
        status = self.letterCheck(user, l.lower())
        if(self.guessCallback):
            self.guessCallback(user.getUsername(), status)
        return status

    def getGameStats(self):
        """ generates game stats """
        users_list=list()
        add_word = self.getCurrentWord()
        if(self.isGameFinished()):
            add_word = self.word
        for u in self.users:
            users_list.append({u.getUsername() : u.getScore()})
        stats={
            "word" : add_word,
            "maxpoints" : self.maximumPoints(),
            "users" : users_list
        }
        return stats

    def gameStateCheck(self):
        # just based on length
        return len(self.lettersInWord) == len(self.guessedLetters)

    def isGameFinished(self):
        return self.gameFinished

    def isGameStarted(self):
        return not self.isGameFinished()
    
    def flagGameEnd(self):
        printf("Game is finished")
        self.gameFinished = True
        if(self.finishCallback):
            self.finishCallback(self.getGameStats())

    def maximumPoints(self):
        suma = 0
        for l in self.lettersInWord:
            suma = suma + HangmanGame.POINTS_FOR_LETTER
        return suma
        
    def getWord(self):
        return self.word

if __name__ == "__main__":
    users = ["marko", "lejla", "Branko"]
    h_users = list()
    for user in users:
        h_users.append(HangmanPlayer(user))

    h = HangmanGame(users=h_users)
    myUser = h_users[2]
    while not h.isGameFinished():
        letter = input().strip()
        if(len(letter) > 2):
            print("Use letter, not word")
            continue
        h.guessLetter(myUser, letter)

    printf("Stats:")
    print(h.getGameStats())
